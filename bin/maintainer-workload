#!/usr/bin/env ruby

require 'docopt'
require 'httparty'
require 'yaml'

docstring = <<DOCSTRING
Show workload (by MRs merged) of current maintainers per release.

This is not exact, because multiple maintainers might review the same
MR, and only the last maintainer assigned will be counted.

Usage:
  #{__FILE__} [--area=<area>] <release>...
  #{__FILE__} -h | --help

Options:
  --area=<area>  The area of expertise (frontend, backend, etc.) [default: backend]
  -h --help      Show this screen.
DOCSTRING

def print_row(*cells)
  puts "| #{cells.join(' | ')} |"
end

def show_maintainer_workload(releases, area)
  maintainers =
    YAML.
      load(File.open('data/team.yml')).
      select { |person| person['projects'] && person['projects']['gitlab-ce'] == "maintainer #{area}" }.
      map { |person| person['gitlab'] }.
      sort_by(&:downcase)

  print_row('Maintainer', *releases)
  print_row(*['---'] * releases.length.succ)

  maintainers.each do |maintainer|
    maintainer_id =
      HTTParty.
        get("https://gitlab.com/api/v4/users?username=#{maintainer}").
        first['id']

    mr_counts = releases.map do |release|
      HTTParty.
        get("https://gitlab.com/api/v4/groups/9970/merge_requests?assignee_id=#{maintainer_id}&milestone=#{release}&state=merged").
        headers['x-total'].
        to_i
    end

    print_row(maintainer, *mr_counts)
  end
end

begin
  options = Docopt::docopt(docstring)
  releases = options.fetch('<release>')
  area = options.fetch('--area')

  show_maintainer_workload(releases, area)
rescue Docopt::Exit => e
  puts e.message
end
